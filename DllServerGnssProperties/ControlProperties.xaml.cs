﻿using DllServerGnssProperties.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using WpfPasswordControlLibrary;

namespace DllServerGnssProperties
{
    /// <summary>
    /// Логика взаимодействия для ControlProperties.xaml
    /// </summary>
    public partial class ControlProperties : UserControl, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Events
        public event EventHandler<LocalProperties> OnLocalPropertiesChanged = (sender, newModel) => { };
        public event EventHandler OnLocalDefaultButtonClick = (sender, obj) => { };
        public event EventHandler<Languages> OnLanguageChanged = (sender, language) => { };
        public event EventHandler<bool> OnPasswordChecked = (sender, isCorrect) => { };
        #endregion

        #region Properties

        private readonly WindowPass _accessWindowPass = new WindowPass();


        private bool isValid;
        public bool IsValid
        {
            get => isValid;
            private set
            {
                if (isValid == value) return;
                isValid = value;
                OnPropertyChanged();
            }
        }

        private string errorMessage;
        public string ErrorMessage
        {
            get => errorMessage;
            set
            {
                if (errorMessage == value) return;
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        private bool _showToolTip;
        public bool ShowToolTip
        {
            get => _showToolTip;
            set
            {
                if (_showToolTip == value) return;
                _showToolTip = value;
                OnPropertyChanged();
            }
        }

        private LocalProperties savedLocal;

        public LocalProperties Local
        {
            get => (LocalProperties)Resources["localProperties"];
            set
            {
                if ((LocalProperties)Resources["localProperties"] == value)
                {
                    savedLocal = value.Clone();
                    return;
                }
                ((LocalProperties)Resources["localProperties"]).Update(value);
                savedLocal = value.Clone();
            }
        }

        #endregion

        #region Language
        Dictionary<string, string> TranslateDic;

        private void ChangeLanguage(Languages newLanguage)
        {
            SetDynamicResources(newLanguage);
            LoadDictionary();
            SetCategoryLocalNames();
        }
        private void SetDynamicResources(Languages newLanguage)
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                switch (newLanguage)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/DllServerGnssProperties;component/Languages/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.RU:
                        dict.Source = new Uri("/DllServerGnssProperties;component/Languages/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.AZ:
                        dict.Source = new Uri("/DllServerGnssProperties;component/Languages/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.SRB:
                        dict.Source = new Uri("/DllServerGnssProperties;component/Languages/StringResource.SRB.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/DllServerGnssProperties;component/Languages/StringResource.EN.xaml",
                                          UriKind.Relative);
                        break;
                }
                Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //TODO
            }
        }

        void LoadDictionary()
        {
            var translation = Properties.Resources.Translation;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);
            TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == Local.General.Language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }
        }



        private void SetCategoryLocalNames()
        {
            try
            {
                foreach (var nameCategory in PropertyLocal.Categories.Select(category => category.Name).ToList())
                {
                    PropertyLocal.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Validation

        private void Validate()
        {
            bool isvalid = true;
            string error = "";
            Action<object> funcValidation = (object obj) =>
            {
                foreach (var property in obj.GetType().GetProperties())
                {
                    foreach (var subProperty in property.PropertyType.GetProperties())
                    {
                        try
                        {
                            if (!(property.Name == "DependencyObjectType" || property.Name == "Dispatcher"))
                            {
                                if (!string.IsNullOrWhiteSpace(Convert.ToString(((IDataErrorInfo)property.GetValue(obj))[subProperty.Name])))
                                {
                                    error += $"{TranslateDic[property.Name]}: {TranslateDic[subProperty.Name]}" + "\n";
                                    isvalid = false;
                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
                IsValid = isvalid;
                ShowToolTip = !isvalid;
                ErrorMessage = error;
                return;
            };


            funcValidation(Local);

        }

        #endregion

        #region Access
        private bool _isAccessChecked = false;

        private void InitAccessWindow()
        {
            _accessWindowPass.OnEnterInvalidPassword += _accessWindowPass_OnEnterInvalidPassword;
            _accessWindowPass.OnEnterValidPassword += _accessWindowPass_OnEnterValidPassword;
            _accessWindowPass.OnClosePasswordBox += _accessWindowPass_OnClosePasswordBox;
            _accessWindowPass.Resources = Resources;
            _accessWindowPass.SetResourceReference(WindowPass.LablePasswordProperty, "labelPassword");
        }

        private void _accessWindowPass_OnEnterValidPassword(object sender, EventArgs e)
        {
            _isAccessChecked = true;
            Local.General.Access = AccessTypes.Admin;
            ChangeVisibilityLocalProperties(true);
            _accessWindowPass.Hide();
            OnPasswordChecked(this, true);
        }

        private void _accessWindowPass_OnClosePasswordBox(object sender, EventArgs e)
        {
            Local.General.Access = AccessTypes.User;
        }

        private void _accessWindowPass_OnEnterInvalidPassword(object sender, EventArgs e)
        {
            Local.General.Access = AccessTypes.User;
            _accessWindowPass.Hide();

            OnPasswordChecked(this, false);
        }

        private void CheckLocalAccess()
        {
            if (!_isAccessChecked)
            {
                Task.Run(() =>
                   Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                   {
                       Local.General.Access = AccessTypes.User;
                       _accessWindowPass.ShowDialog();
                   }));
            }
            else
                _isAccessChecked = false;
        }

        public void SetPassword(string password)
        {
            _accessWindowPass.ValidPassword = password;
        }

        #endregion

        #region Local Global methods

        private void InitLocalProperties()
        {
            savedLocal = Local.Clone();
            savedLocal.General.PropertyChanged += SavedLocal_PropertyChanged;
            Local.General.PropertyChanged += Local_PropertyChanged;
            Local.OnPropertyChanged += OnPropertyChanged;

            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.General), typeof(LocalProperties), typeof(Common)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Server), typeof(LocalProperties), typeof(EndPointConnection)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Gps), typeof(LocalProperties), typeof(Gps)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Glonass), typeof(LocalProperties), typeof(Glonass)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Testing), typeof(LocalProperties), typeof(Testing)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Database), typeof(LocalProperties), typeof(EndPointConnection)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.MosaicX5), typeof(LocalProperties), typeof(MosaicX5)));
        }



        private void Local_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            switch (e.PropertyName)
            {
                case nameof(Common.Language):
                    ChangeLanguage(Local.General.Language);
                    OnLanguageChanged(sender, Local.General.Language);

                    break;
                case nameof(Common.Access):
                    Console.Write($"Access: {Local.General.Access} \n");
                    Console.Write($"IsAccessChecked: {_isAccessChecked} \n");
                    if (Local.General.Access == AccessTypes.Admin)
                    {
                        CheckLocalAccess();
                        Console.WriteLine($"Acsess change to Admin");
                    }
                    else
                    {
                        ChangeVisibilityLocalProperties(false);
                        Console.WriteLine($"Acsess change to User");
                    }
                    Console.WriteLine(Local.General.Access);


                    break;
                default:
                    break;
            }
        }

        private void SavedLocal_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Common.Language):
                    OnLanguageChanged(sender, savedLocal.General.Language);
                    ChangeLanguage(savedLocal.General.Language);
                    break;

                default:
                    break;
            }

        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Validate();
        }

        #endregion

        public ControlProperties()
        {
            InitializeComponent();
            InitLocalProperties();
            ChangeLanguage(savedLocal.General.Language);
            InitAccessWindow();
        }

        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            if (!savedLocal.EqualTo(Local))
            {
                savedLocal.Update(Local.Clone());
                OnLocalPropertiesChanged(this, savedLocal.Clone());
            }
        }



        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Validate();
        }

        private void ButDefault_Click(object sender, RoutedEventArgs e)
        {
            OnLocalDefaultButtonClick(this, null);
        }

        public void UpdateComPorts()
        {
            (Resources["PortsNames"] as ComPortsNames).UpdatePort();
        }

        public void ChangeVisibilityLocalProperties(bool browsable)
        {
            PropertyLocal.Properties[nameof(LocalProperties.Server)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.Database)].IsBrowsable = browsable;
            //PropertyLocal.Properties[nameof(LocalProperties.Gps)].IsBrowsable = browsable;
            if (browsable)
            {
                PropertyLocal.Properties[nameof(LocalProperties.Glonass)].IsBrowsable = browsable;
                PropertyLocal.Properties[nameof(LocalProperties.Gps)].IsBrowsable = browsable;
                PropertyLocal.Properties[nameof(LocalProperties.MosaicX5)].IsBrowsable = browsable;
            }
            else
            {
                PropertyLocal.Properties[nameof(LocalProperties.Glonass)].IsBrowsable = Local.Glonass.Existance;
                PropertyLocal.Properties[nameof(LocalProperties.Gps)].IsBrowsable = Local.Gps.Existance;
                PropertyLocal.Properties[nameof(LocalProperties.MosaicX5)].IsBrowsable = Local.MosaicX5.Existance;
            }
            PropertyLocal.Properties[nameof(LocalProperties.Testing)].IsBrowsable = browsable;
        }



        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
            ChangeVisibilityLocalProperties(false);
        }
    }
}
