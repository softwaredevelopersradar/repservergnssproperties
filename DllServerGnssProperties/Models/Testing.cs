﻿
using System.ComponentModel;

namespace DllServerGnssProperties.Models
{
    public class Testing : AbstractBaseProperties<Testing>
    {
        #region Model Methods

        public override bool EqualTo(Testing model)
        {
            return AccumulateFiles == model.AccumulateFiles
                && WriteSatsLogToTxt == model.WriteSatsLogToTxt;
                //&& WaitForAlmanac == model.WaitForAlmanac
        }

        public override Testing Clone()
        {
            return new Testing
            {
                AccumulateFiles = AccumulateFiles,
                WriteSatsLogToTxt = WriteSatsLogToTxt,
                //WaitForAlmanac = WaitForAlmanac
            };
        }

        public override void Update(Testing model)
        {
            AccumulateFiles = model.AccumulateFiles;
            WriteSatsLogToTxt = model.WriteSatsLogToTxt;
            //WaitForAlmanac = model.WaitForAlmanac;
        }

        #endregion


        private bool accumulateFiles;
        private bool writeSatsLogToTxt;
        //private bool waitForAlmanac;


        [NotifyParentProperty(true)]
        public bool AccumulateFiles
        {
            get => accumulateFiles;
            set
            {
                if (accumulateFiles == value) return;
                accumulateFiles = value;
                OnPropertyChanged();
            }
        }



        [NotifyParentProperty(true)]
        public bool WriteSatsLogToTxt
        {
            get { return writeSatsLogToTxt; }
            set
            {
                if (writeSatsLogToTxt == value) return;
                writeSatsLogToTxt = value;
                OnPropertyChanged();
            }
        }



        //[NotifyParentProperty(true)]
        //public bool WaitForAlmanac
        //{
        //    get { return waitForAlmanac; }
        //    set
        //    {
        //        if (waitForAlmanac == value) return;
        //        waitForAlmanac = value;
        //        OnPropertyChanged();
        //    }
        //}
    }
}
