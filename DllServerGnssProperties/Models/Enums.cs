﻿using System.ComponentModel;

namespace DllServerGnssProperties.Models
{
    public enum Languages : byte
    {
        [Description("Русский")]
        RU,
        [Description("English")]
        EN,
        [Description("Azərbaycan")]
        AZ,
        [Description("Српски")]
        SRB,
    }

    public enum AccessTypes : byte
    {
        Admin,
        User
    }

    public enum Geodata: byte
    {
        GPS = 1,
        GLONASS = 2
    }


    public enum ViewCoord : byte
    {
        [Description("DD.dddddd")]
        Dd = 1,
        [Description("DD MM.mmmm")]
        DMm = 2,
        [Description("DD MM SS.ss")]
        DMSs = 3
    }

    public enum Version : byte
    {
        V3 = 0,
        V5 = 1,
    }
}
