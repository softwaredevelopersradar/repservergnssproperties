﻿using System.Collections.ObjectModel;

namespace DllServerGnssProperties.Models
{
    public class ComPortsSpeed : ObservableCollection<int>
    {
        public ComPortsSpeed()
        {
            Add(2400);
            Add(4800);
            Add(9600);
            Add(19200);
            Add(38400);
            Add(57600);
            Add(115200);
        }
    }
}
