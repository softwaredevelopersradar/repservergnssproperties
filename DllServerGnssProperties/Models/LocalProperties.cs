﻿
using System.ComponentModel;
using System.Windows.Controls.WpfPropertyGrid;

namespace DllServerGnssProperties.Models
{
    [CategoryOrder(nameof(General), 1)]
    [CategoryOrder(nameof(Server), 2)]
    [CategoryOrder(nameof(Gps), 3)]
    [CategoryOrder(nameof(Glonass), 4)]
    [CategoryOrder(nameof(Testing), 5)]
    [CategoryOrder(nameof(Database), 6)]
    [CategoryOrder(nameof(MosaicX5), 7)]
    public class LocalProperties : IModelMethods<LocalProperties>
    {
        public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };
        public LocalProperties()
        {
            General = new Common();
            Server = new EndPointConnection();
            Gps = new Gps();
            Glonass = new Glonass();
            Testing = new Testing();
            Database = new EndPointConnection();
            MosaicX5 = new MosaicX5();
            

            General.PropertyChanged += PropertyChanged;
            Server.PropertyChanged += PropertyChanged;
            Gps.PropertyChanged += PropertyChanged;
            Glonass.PropertyChanged += PropertyChanged;
            Testing.PropertyChanged += PropertyChanged;
            Database.PropertyChanged += this.PropertyChanged;
            MosaicX5.PropertyChanged +=PropertyChanged;
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
          OnPropertyChanged(this, e);
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(General))]
        [DisplayName(" ")]
        public Common General { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Server))]
        [DisplayName(" ")]
        public EndPointConnection Server { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Gps))]
        [DisplayName(" ")]
        public Gps Gps { get; set; }


        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Glonass))]
        [DisplayName(" ")]
        public Glonass Glonass { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Testing))]
        [DisplayName(" ")]
        public Testing Testing { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Database))]
        [DisplayName(" ")]
        public EndPointConnection Database { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(MosaicX5))]
        [DisplayName(" ")]
        public MosaicX5 MosaicX5 { get; set; }

        #region Model Methods

        public LocalProperties Clone()
        {
            return new LocalProperties
            {
                General = General.Clone(),
                Server = Server.Clone(),
                Gps = Gps.Clone(),
                Glonass = Glonass.Clone(),
                Testing = Testing.Clone(),
                Database = Database.Clone(),
                MosaicX5 = MosaicX5.Clone(),
            };
        }

        public void Update(LocalProperties localProperties)
        {
            General.Update(localProperties.General);
            Server.Update(localProperties.Server);
            Gps.Update(localProperties.Gps);
            Glonass.Update(localProperties.Glonass);
            Testing.Update(localProperties.Testing);
            Database.Update(localProperties.Database);
            MosaicX5.Update(localProperties.MosaicX5);
        }

        public bool EqualTo(LocalProperties localProperties)
        {
            return General.EqualTo(localProperties.General)
                && Server.EqualTo(localProperties.Server)
                && Gps.EqualTo(localProperties.Gps)
                && Glonass.EqualTo(localProperties.Glonass)
                && Testing.EqualTo(localProperties.Testing)
                && Database.EqualTo(localProperties.Database)
                && MosaicX5.EqualTo(localProperties.MosaicX5);
        }
        #endregion
    }
}
