﻿using System.ComponentModel;

namespace DllServerGnssProperties.Models
{
    public class Glonass : AbstractBaseProperties<Glonass>
    {
        #region ModelMethods

        public override bool EqualTo(Glonass model)
        {
            return Existance == model.Existance &&
            ComPort == model.ComPort &&
            ComPortSpeed == model.ComPortSpeed &&
            FolderAlmanacGlonass == model.FolderAlmanacGlonass &&
            FolderEphemerisGlonass == model.FolderEphemerisGlonass &&
            IntervalData == model.IntervalData &&
            IntervalCoordUpdate == model.IntervalCoordUpdate &&
            FolderAlmanacGps == model.FolderAlmanacGps &&
            FolderEphemerisGps == model.FolderEphemerisGps &&
            EphemerisFileNameGlonass == model.EphemerisFileNameGlonass &&
            AlmanacFileNameGlonass == model.AlmanacFileNameGlonass &&
            EphemerisFileNameGps == model.EphemerisFileNameGps &&
            AlmanacFileNameGps == model.AlmanacFileNameGps &&
            Version == model.Version;
        }

        public override Glonass Clone()
        {
            return new Glonass
            {
                Existance = Existance,
                ComPort = ComPort,
                ComPortSpeed = ComPortSpeed,
                FolderAlmanacGlonass = FolderAlmanacGlonass,
                FolderEphemerisGlonass = FolderEphemerisGlonass,
                IntervalData = IntervalData,
                IntervalCoordUpdate = IntervalCoordUpdate,
                FolderAlmanacGps = FolderAlmanacGps,
                FolderEphemerisGps = FolderEphemerisGps,
                EphemerisFileNameGlonass = EphemerisFileNameGlonass,
                AlmanacFileNameGlonass = AlmanacFileNameGlonass,
                EphemerisFileNameGps = EphemerisFileNameGps,
                AlmanacFileNameGps = AlmanacFileNameGps,
                Version = Version
            };

        }

        public override void Update(Glonass model)
        {
            Existance = model.Existance;
            ComPort = model.ComPort;
            ComPortSpeed = model.ComPortSpeed;
            FolderAlmanacGlonass = model.FolderAlmanacGlonass;
            FolderEphemerisGlonass = model.FolderEphemerisGlonass;
            IntervalData = model.IntervalData;
            IntervalCoordUpdate = model.IntervalCoordUpdate;
            FolderAlmanacGps = model.FolderAlmanacGps;
            FolderEphemerisGps = model.FolderEphemerisGps;
            EphemerisFileNameGlonass = model.EphemerisFileNameGlonass;
            AlmanacFileNameGlonass = model.AlmanacFileNameGlonass;
            EphemerisFileNameGps = model.EphemerisFileNameGps;
            AlmanacFileNameGps = model.AlmanacFileNameGps;
            Version = model.Version;
        }

        #endregion

        private bool existance;
        private int comPortSpeed;
        private string comPort;
        private string folderAlmanacGlonass = "";
        private string folderEphemerisGlonass = "";
        private string folderAlmanacGps = "";
        private string folderEphemerisGps = "";
        private int intervalData;
        private int intervalCoordUpdate;
        private Version version = Models.Version.V3;

        private string almanacFileNameGlonass = "currentGlonass.Agl";
        private string ephemerisFileNameGlonass = "currentGlonass.rnx";
        private string almanacFileNameGps = "currentGps.AL3";
        private string ephemerisFileNameGps = "currentGps.15n";


        [NotifyParentProperty(true)]
        public bool Existance
        {
            get => existance;
            set
            {
                if (value == existance) return;
                existance = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string ComPort
        {
            get => comPort;
            set
            {
                if (comPort == value) return;
                comPort = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public int ComPortSpeed
        {
            get => comPortSpeed;
            set
            {
                if (comPortSpeed == value)
                    return;
                comPortSpeed = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public int IntervalData
        {
            get => intervalData;
            set
            {
                if (intervalData == value)
                    return;
                intervalData = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string FolderAlmanacGlonass
        {
            get => folderAlmanacGlonass;
            set
            {
                if (folderAlmanacGlonass == value)
                    return;
                folderAlmanacGlonass = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string FolderEphemerisGlonass
        {
            get => folderEphemerisGlonass;
            set
            {
                if (folderEphemerisGlonass == value)
                    return;
                folderEphemerisGlonass = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string FolderAlmanacGps
        {
            get => folderAlmanacGps;
            set
            {
                if (folderAlmanacGps == value)
                    return;
                folderAlmanacGps = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string FolderEphemerisGps
        {
            get => folderEphemerisGps;
            set
            {
                if (folderEphemerisGps == value)
                    return;
                folderEphemerisGps = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public int IntervalCoordUpdate
        {
            get => intervalCoordUpdate;
            set
            {
                if (intervalCoordUpdate == value)
                    return;
                intervalCoordUpdate = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string AlmanacFileNameGlonass
        {
            get { return almanacFileNameGlonass; }
            set
            {
                if (almanacFileNameGlonass == value) return;
                almanacFileNameGlonass = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string EphemerisFileNameGlonass
        {
            get { return ephemerisFileNameGlonass; }
            set
            {
                if (ephemerisFileNameGlonass == value) return;
                ephemerisFileNameGlonass = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string AlmanacFileNameGps
        {
            get { return almanacFileNameGps; }
            set
            {
                if (almanacFileNameGps == value) return;
                almanacFileNameGps = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string EphemerisFileNameGps
        {
            get { return ephemerisFileNameGps; }
            set
            {
                if (ephemerisFileNameGps == value) return;
                ephemerisFileNameGps = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public Version Version
        {
            get { return version; }
            set
            {
                if (version == value) return;
                version = value;
                OnPropertyChanged();
            }
        }
    }
}
