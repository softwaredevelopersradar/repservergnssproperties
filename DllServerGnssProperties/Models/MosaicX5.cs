﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllServerGnssProperties.Models
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class MosaicX5 : AbstractBaseProperties<MosaicX5>
    {
        #region ModelMethods

        public override bool EqualTo(MosaicX5 model)
        {
            return Existance == model.Existance && ComPort == model.ComPort && ComPortSpeed == model.ComPortSpeed
                   && FolderAlmanac == model.FolderAlmanac && FolderEphemeris == model.FolderEphemeris &&
                   //LifetimeAlmanac == model.LifetimeAlmanac &&
                   //IntervalAlmanac == model.IntervalAlmanac &&
                   //IntervalEphemeris == model.IntervalEphemeris &&
                   IntervalCoordUpdate == model.IntervalCoordUpdate &&
                   //AutoControlEpochChange == model.AutoControlEpochChange &&
                   AlmanacFileName == model.AlmanacFileName && EphemerisFileName == model.EphemerisFileName;
            //SatelliteFilter == model.SatelliteFilter &&
            //BadEphemerisTime == model.BadEphemerisTime;
        }

        public override MosaicX5 Clone()
        {
            return new MosaicX5
            {
                Existance = Existance,
                ComPort = ComPort,
                ComPortSpeed = ComPortSpeed,
                FolderAlmanac = FolderAlmanac,
                FolderEphemeris = FolderEphemeris,
                //LifetimeAlmanac = LifetimeAlmanac,
                //IntervalAlmanac = IntervalAlmanac,
                //IntervalEphemeris = IntervalEphemeris,
                IntervalCoordUpdate = IntervalCoordUpdate,
                //AutoControlEpochChange = AutoControlEpochChange,

                AlmanacFileName = AlmanacFileName,
                EphemerisFileName = EphemerisFileName,
                //SatelliteFilter = SatelliteFilter,
                //BadEphemerisTime = BadEphemerisTime
            };

        }

        public override void Update(MosaicX5 model)
        {
            Existance = model.Existance;
            ComPort = model.ComPort;
            ComPortSpeed = model.ComPortSpeed;
            FolderAlmanac = model.FolderAlmanac;
            FolderEphemeris = model.FolderEphemeris;
            //LifetimeAlmanac = model.LifetimeAlmanac;
            //IntervalAlmanac = model.IntervalAlmanac;
            //IntervalEphemeris = model.IntervalEphemeris;
            IntervalCoordUpdate = model.IntervalCoordUpdate;
            //AutoControlEpochChange = model.AutoControlEpochChange;

            AlmanacFileName = model.AlmanacFileName;
            EphemerisFileName = model.EphemerisFileName;
            //SatelliteFilter = model.SatelliteFilter;
            //BadEphemerisTime = model.BadEphemerisTime;
        }

        #endregion

        private bool existance;
        private int comPortSpeed;
        private string comPort;
        private string folderAlmanac = "";
        private string folderEphemeris = "";
        //private int lifetimeAlmanac;
        //private int intervalAlmanac = 20;
        //private int intervalEphemeris;
        private int intervalCoordUpdate;
        //private bool autoControlEpochChange;
        //private int badEphemerisTime = 10;

        private string almanacFileName = "current.AL3";
        private string ephemerisFileName = "current.15n";
       


        [NotifyParentProperty(true)]
        public bool Existance
        {
            get => existance;
            set
            {
                if (value == existance) return;
                existance = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string ComPort
        {
            get => comPort;
            set
            {
                if (comPort == value) return;
                comPort = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public int ComPortSpeed
        {
            get => comPortSpeed;
            set
            {
                if (comPortSpeed == value)
                    return;
                comPortSpeed = value;
                OnPropertyChanged();
            }
        }

        //[NotifyParentProperty(true)]
        //public int LifetimeAlmanac
        //{
        //    get => lifetimeAlmanac;
        //    set
        //    {
        //        if (lifetimeAlmanac == value)
        //            return;
        //        lifetimeAlmanac = value;
        //        OnPropertyChanged();
        //    }
        //}


        //[NotifyParentProperty(true)]
        //public int IntervalAlmanac
        //{
        //    get => intervalAlmanac;
        //    set
        //    {
        //        if (intervalAlmanac == value)
        //            return;
        //        intervalAlmanac = value;
        //        OnPropertyChanged();
        //    }
        //}


        //[NotifyParentProperty(true)]
        //public int IntervalEphemeris
        //{
        //    get => intervalEphemeris;
        //    set
        //    {
        //        if (intervalEphemeris == value)
        //            return;
        //        intervalEphemeris = value;
        //        OnPropertyChanged();
        //    }
        //}


        [NotifyParentProperty(true)]
        public string FolderAlmanac
        {
            get => folderAlmanac;
            set
            {
                if (folderAlmanac == value)
                    return;
                folderAlmanac = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string FolderEphemeris
        {
            get => folderEphemeris;
            set
            {
                if (folderEphemeris == value)
                    return;
                folderEphemeris = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public int IntervalCoordUpdate
        {
            get => intervalCoordUpdate;
            set
            {
                if (intervalCoordUpdate == value)
                    return;
                intervalCoordUpdate = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string AlmanacFileName
        {
            get { return almanacFileName; }
            set
            {
                if (almanacFileName == value) return;
                almanacFileName = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string EphemerisFileName
        {
            get { return ephemerisFileName; }
            set
            {
                if (ephemerisFileName == value) return;
                ephemerisFileName = value;
                OnPropertyChanged();
            }
        }


        //[NotifyParentProperty(true)]
        //public bool SatelliteFilter
        //{
        //    get { return satelliteFilter; }
        //    set
        //    {
        //        if (satelliteFilter == value) return;
        //        satelliteFilter = value;
        //        OnPropertyChanged();
        //    }
        //}

        //[NotifyParentProperty(true)]
        //public bool AutoControlEpochChange
        //{
        //    get => autoControlEpochChange;
        //    set
        //    {
        //        if (value == autoControlEpochChange) return;
        //        autoControlEpochChange = value;
        //        OnPropertyChanged();
        //    }
        //}

        //[NotifyParentProperty(true)]
        //[Range(0, 15)]
        //public int BadEphemerisTime
        //{
        //    get => badEphemerisTime;
        //    set
        //    {
        //        if (badEphemerisTime == value)
        //            return;
        //        if (value > 15 || value < 0) badEphemerisTime = 10;
        //        else badEphemerisTime = value;
        //        OnPropertyChanged();
        //    }
        //}
    }
}
