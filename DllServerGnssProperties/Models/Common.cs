﻿using System;

namespace DllServerGnssProperties.Models
{
    public class Common: AbstractBaseProperties<Common>
    { 
        #region IModelMethods

        public override bool EqualTo(Common model)
        {
            return Access == model.Access
                && Language == model.Language
                && Geodata == model.Geodata
                && TimeZone == model.TimeZone
                && AutoUpdateTime == model.AutoUpdateTime
                && IntervalTimeUpdate == model.IntervalTimeUpdate
                && IsVisibleEN == model.IsVisibleEN
                && IsVisibleRU == model.IsVisibleRU
                && IsVisibleAZ == model.IsVisibleAZ
                && IsVisibleSRB == model.IsVisibleSRB
                && CoordinateView == model.CoordinateView;
        }

        public override Common Clone()
        {
            return new Common
            {
                Access = Access,
                Language = Language,
                Geodata = Geodata,
                TimeZone = TimeZone,
                AutoUpdateTime = AutoUpdateTime,
                IntervalTimeUpdate = IntervalTimeUpdate,
                CoordinateView = CoordinateView,
                IsVisibleEN = IsVisibleEN,
                IsVisibleRU = IsVisibleRU,
                IsVisibleAZ = IsVisibleAZ,
                IsVisibleSRB = IsVisibleSRB
        };
        }

        public override void Update(Common model)
        {
            Access = model.Access;
            Language = model.Language;
            Geodata = model.Geodata;
            CoordinateView = model.CoordinateView;
            TimeZone = model.TimeZone;
            AutoUpdateTime = model.AutoUpdateTime;
            IntervalTimeUpdate = model.IntervalTimeUpdate;
            IsVisibleEN = model.IsVisibleEN;
            IsVisibleRU = model.IsVisibleRU;
            IsVisibleAZ = model.IsVisibleAZ;
            IsVisibleSRB = model.IsVisibleSRB;
        }

        #endregion

        private AccessTypes access = AccessTypes.User;
        private Languages language;
        private Geodata geodata = Geodata.GPS;
        private ViewCoord coordinateView = ViewCoord.Dd;
        private string timeZone;
        private bool autoUpdateTime;
        private int intervalTimeUpdate;

        private bool en = true;
        private bool ru = true;
        private bool az = true;
        private bool srb = true;

        public AccessTypes Access
        {
            get => access;
            set
            {
                if (access == value) return;
                access = value;
                Console.WriteLine($"Accsess: {access}");
                OnPropertyChanged();
            }
        }

        public Languages Language
        {
            get => language;
            set
            {
                if (language == value) return;
                language = value;
                OnPropertyChanged();
            }
        }

        public Geodata Geodata
        {
            get => geodata;
            set
            {
                if (value == geodata) return;
                geodata = value;
                OnPropertyChanged();
            }
        }


        public ViewCoord CoordinateView 
        {
            get => coordinateView;
            set
            {
                if (value == coordinateView) return;
                coordinateView = value;
                OnPropertyChanged();
            }
        }


        public bool AutoUpdateTime
        {
            get => autoUpdateTime; 
            set
            {
                if (autoUpdateTime == value)
                    return;
                autoUpdateTime = value;
                OnPropertyChanged();
            }
        }

        public string TimeZone
        {
            get
            {
                TimeZoneInfo.ClearCachedData();
                return TimeZoneInfo.Local.DisplayName;
            }
            set
            {
                OnPropertyChanged();
            }
        }

        public int IntervalTimeUpdate
        {
            get => intervalTimeUpdate; 
            set
            {
                if (intervalTimeUpdate == value)
                    return;
                intervalTimeUpdate = value;
                OnPropertyChanged();
            }
        }

        public bool IsVisibleEN
        {
            get => en;
            set
            {             
                if (en == value) return;
                en = value;                
                OnPropertyChanged();
            }
        }

        public bool IsVisibleRU
        {
            get => ru;
            set
            {
                if (ru == value) return;
                ru = value;
                OnPropertyChanged();
            }
        }

        public bool IsVisibleAZ
        {
            get => az;
            set
            {
                if (az == value) return;
                az = value;               
                OnPropertyChanged();
            }
        }

        public bool IsVisibleSRB
        {
            get => this.srb;
            set
            {
                if (srb == value) return;
                srb = value;
                OnPropertyChanged();
            }
        }
    }
}
