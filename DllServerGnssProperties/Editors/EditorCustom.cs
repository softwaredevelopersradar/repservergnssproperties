﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using DllServerGnssProperties.Models;

namespace DllServerGnssProperties.Editors
{
    public class EditorCustom : PropertyEditor
    {
        Dictionary<Type, string> dictKeyDataTemplate = new Dictionary<Type, string>()
        {
            { typeof(EndPointConnection), "EndPointEditorKey"},
            { typeof(Common), "CommonLocalEditorKey"},
            { typeof(Glonass),  "GlonassEditorKey"},
            { typeof(Gps), "GpsEditorKey" },
            { typeof(Testing), "TestingEditorKey" },
            { typeof(MosaicX5), "MosaicX5EditorKey" }
        };

        public EditorCustom(string PropertyName, Type DeclaringType, Type typeProperty)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/DllServerGnssProperties;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[typeProperty]];
        }

        public EditorCustom(string PropertyName, Type DeclaringType, string nameEditorKey)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/DllServerGnssProperties;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[nameEditorKey];
        }
    }
}
