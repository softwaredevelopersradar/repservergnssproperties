﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DllServerGnssProperties.Converters
{
    public class VersionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Models.Version)System.Convert.ToByte(value);
        }
    }
}
