﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DllServerGnssProperties.Converters
{
    public class GeodataTypesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Models.Geodata)System.Convert.ToByte(value);
        }
    }
}
