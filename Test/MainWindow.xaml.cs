﻿using DllServerGnssProperties.Models;
using System;
using System.Windows;

namespace Test
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            prop.SetPassword("123");
            prop.Local.General.Access = AccessTypes.Admin;
        }

        private void Prop_OnLocalDefaultButtonClick(object sender, EventArgs e)
        {
            MessageBox.Show("LocalDefault!");
        }

        private void Prop_OnGlobalDefaultButtonClick(object sender, EventArgs e)
        {
            MessageBox.Show("GlobalDefault!");

        }

        private void Prop_OnLanguageChanged(object sender, DllServerGnssProperties.Models.Languages e)
        {
            MessageBox.Show(e.ToString());
        }

        private void prop_OnLocalPropertiesChanged(object sender, DllServerGnssProperties.Models.LocalProperties e)
        {
            //int f = 6;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            prop.UpdateComPorts();
        }

        private void Prop_OnPasswordChecked(object sender, bool e)
        {
            if (e)
            {
                MessageBox.Show("Good job!");
            }
            else
            {
                MessageBox.Show("Invalid password!");
            }
        }
    }
}
